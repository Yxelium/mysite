function progressBar() {
    let scroll = document.body.scrollTop || document.documentElement.scrollTop;
    let height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
    let scrolled = scroll / height * 100;

    document.getElementById('progressBar').style.width = scrolled + '%';
}

window.addEventListener('scroll', progressBar);


var lastScrollPosition = 0; 

$('#scroll-up').click( function(){
	if ( $(document).scrollTop() > 0 ) {
		$('body').animate({scrollTop:0},1000);
		lastScrollPosition = $(document).scrollTop();
	} else {
		$('body').animate({scrollTop:lastScrollPosition},1000);
	}	
});

$(document).scroll( function() {
	if ( $(document).scrollTop() > 0 ) {
		$('#scroll-up').fadeIn();
		$('#scroll-up').text('Наверх');
	} else {
		$('#scroll-up').text('Вниз');
	}
});